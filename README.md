# 作业1

#### 介绍



1.7  任务一








































搭建模型，要求设置两层隐层：
•第一层隐层设置：神经元个数256，初始化方法为glorot_normal，激活函数为tanh
•第二层隐层设置：神经元个数128，初始化方法为glorot_normal，激活函数为tanh

然后，运行模型训练和测试集评估代码。

































1.8  任务二
















































请尝试使用课上讲的提前终止的方法解决过拟合，使用到的api如下：

tf.keras.callbacks.EarlyStopping

用到的参数：
•monitor：监控的数据，一般为'val_loss'。
•min_delta：定义模型改善的最小量，只有大于min_delta才会认为模型有改善，默认为0。
•patience：有多少个epoch，模型没有改善，训练就会停止，默认为0。
•restore_best_weights：是否使用监控数据最

#### 软件架构
软件架构说明


#### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
