# 作业1

#### Description



1.7  任务一








































搭建模型，要求设置两层隐层：
•第一层隐层设置：神经元个数256，初始化方法为glorot_normal，激活函数为tanh
•第二层隐层设置：神经元个数128，初始化方法为glorot_normal，激活函数为tanh

然后，运行模型训练和测试集评估代码。

































1.8  任务二
















































请尝试使用课上讲的提前终止的方法解决过拟合，使用到的api如下：

tf.keras.callbacks.EarlyStopping

用到的参数：
•monitor：监控的数据，一般为'val_loss'。
•min_delta：定义模型改善的最小量，只有大于min_delta才会认为模型有改善，默认为0。
•patience：有多少个epoch，模型没有改善，训练就会停止，默认为0。
•restore_best_weights：是否使用监控数据最

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
